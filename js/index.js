'use strict';
((D, B, log = arg => console.log(arg)) => {
    //D = document, B = document.body
    //log = console.log()

//********************************************** .our .tabs-filter *****************************************************

  const remAct = (e) => {D.querySelectorAll(e).forEach((elem) => {if (elem.closest('.active')) elem.classList.remove('active')})}, //зняття класу active для .our
        setAct = (e) => {D.querySelectorAll(`[data-name=${e}]`).forEach((e) => {e.classList.add('active')})}, //встановлення класу active для .our
        remImg = (e) => {D.querySelectorAll(`${e}`).forEach((e) => {e.remove()})}, //видалення картинок .our
        setBlock = (div) => {                                //установка блоку для hover-у для .tabs-filter
            div.insertAdjacentHTML('afterbegin',
                    '<div class="block">' +
                    '<svg width="88" height="43" viewBox="0 0 88 43" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                    '<g clip-path="url(#clip0_2143_233)">' +
                    '<rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB"/>' +
                    '<path fill-rule="evenodd" clip-rule="evenodd" d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z" fill="#1FDAB5"/>' +
                    '<path fill-rule="evenodd" clip-rule="evenodd" d="M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z" fill="#18CFAB"/>' +
                    '<rect x="60" y="17" width="12" height="11" fill="white"/>' +
                    '</g>' +
                    '<defs>' +
                    '<clipPath id="clip0_2143_233">' +
                    '<rect width="88" height="43" fill="white"/>' +
                    '</clipPath>' +
                    '</defs>' +
                    '</svg>' +
                    '<a href="#"><span>creative design</span></a>' +
                    '<span>Web Design</span>' +
                    '</div>');
        },
        enter = (e) => {
            if(e.target.children[0].closest('.block')){e.target.children[0].remove()} //блок створюється при наведенні та видаляється коли курсор йде з wrap-ру,
                 if (e.target.closest('.wrap')) {                                     // якщо швидко рухати курсором може утворитися зайвий блок тоді з двох видаляється один,
                         setBlock(e.target);                                //а один залишається тому про всяк випадок перевіряємо і якщо блок повішений ще - видаляємо його
                         e.target.children[1].classList.add('active');
                 }
        },
        leave = (e) => {
            let block,img
            if (e.target.closest('.wrap')) {
                  block = e.target.children[0];
                    img = e.target.children[1];
                block.animate([
                    { transform: 'rotateY(90deg)' }
                ], {
                    duration: 840, //даємо трохи більше часу на анімацію, щоб не проскакував зайвий кадр
                    easing: 'ease-in-out'
                })
            }
            setTimeout(()=>{img.classList.remove('active');block.remove();},800); //після закінчення анімації видаляємо блок і знімаємо актив з картинки
        },
        underImgLink = () => {                                     //hover для .tabs-filter
            const fil = D.querySelectorAll('.wrap');
            fil.forEach((e) => {e.addEventListener('mouseenter',enter)});
            fil.forEach((e) => {e.addEventListener('mouseleave',leave)});
        },
        setImgAll = {
            1:() => {setImg(10, 'wordpress', 'w')},
            2:() => {setImg(12, 'graphic_design', 'gd')},
            3:() => {setImg(7, 'web_design', 'wd', 2)},
            4:() => {setImg(7, 'landing_page', 'lp', 2)},
            5:() => {D.querySelector('.btn-load').style.display = 'none'},
            6:() => {
                for(let i = 1; i <= 5; i++) {   //вивантажує всі фото
                    setImgAll[i]()
                }
            }
        },
        selectTabsFilterList = (event) => {
          if (event.target.closest('.tabs-filter-list')) {
           B.removeEventListener('click', selectTabsFilterList);  //видаляємо слухач подій до завершення завантаження картинок
            D.querySelector('.btn-load').style.display = 'inline-block';
             remAct('.tabs-filter-list');
              setAct(event.target.getAttribute('data-name'));
               remImg(`.wrap`);
                switch (event.target.getAttribute('data-name')) {
                    case 'wordpress':      return setImgAll[1]();  //при натисканні на вибраний таб завантажуємо категорію відповідну йому
                    case 'graphic_design': return setImgAll[2]();
                    case 'web_design':     return setImgAll[3]();
                    case 'landing_pages':  return setImgAll[4]();
                    case 'all':            return setImgAll[6]();
                }
            }
          if (event.target.closest('.btn-load')) {            //при натисканні на кнопку Load More вивантажуємо всі категорії, крім активної
              const x = D.querySelector('.tabs-filter-list.active').getAttribute('data-name')
              if(!(x === 'wordpress' || x === 'all')) {  setImgAll[1]()  }
              if(!(x === 'graphic_design'))           {  setImgAll[2]()  }
              if(!(x === 'web_design'))               {  setImgAll[3]()  }
              if(!(x === 'landing_pages'))            {  setImgAll[4]()  }
          }
            D.querySelector('.btn-load').style.display = 'none';  //приховуємо кнопку
        },
        selectMenuOurList = (event) => {
            if (event.target.closest('.menu-our-list')) {
                remAct('.menu-our-list');
                remAct('.our-img');
                remAct('.description');
                setAct(event.target.getAttribute('data-name'));
            }
        }

function setImg(quantity, catalog, imgName, gridRows = 2, i = 1){            //встановлення картинок для .tabs-filter
      let id = setTimeout(setImg, 250, quantity, catalog, imgName, gridRows, i + 1);
          if (i >= quantity) {
                 clearTimeout(id);
                   B.addEventListener('click', selectTabsFilterList); //після завершення завантаження картинок повертаємо слухач подій
             }
             const tabsFilterContent = D.querySelector('.tabs-filter-content'),
                      div = D.createElement('div'),
                         pic = new Image();
                         pic.src = `./images/Amazing_Work/${catalog}/${imgName}(${i}).jpg`;//картинки завантажуємо з різних каталогів,
                         pic.classList.add('filter-img');                 //з різними іменами, кожна, яка перераховується тільки у своєму каталозі
                         pic.setAttribute('alt',catalog)
                      div.classList.add('wrap');
                      div.style.position = 'relative';
                      div.appendChild(pic);
                   tabsFilterContent.style.setProperty(`grid-template-Rows`, `repeat(${gridRows}, 210px)`);
                   tabsFilterContent.append(div);
       underImgLink();
    }

    setImg(10, 'wordpress', 'w'); //завантаження за замовчуванням для .tabs-filter

    B.addEventListener('click', selectTabsFilterList); //         .tabs-filter
    B.addEventListener('click', selectMenuOurList); //                .our (tabs)

//********************************************** end .our .tabs-filter *************************************************
//********************************************** .field ****************************************************************

    function setBlockField(div, x) {          //новинні блоки генеруються і розміщуються в div.field-content. спочатку розмітки html немає
        div.insertAdjacentHTML('afterbegin', '<a href="#">' +
            '<div class="cell">' +
            '<p>' +
            '<Span>12</Span>' +
            '<span>Feb</span>' +
            '</p>' +
            `<img src="./images/breaking_news/Layer${x}.png" alt="breaking news"/>` +
            '<span>Amazing Blog Post</span>' +
            '<span>By admin&nbsp;|&nbsp;2 comment</span>' +
            '</div>' +
            '</a>');
    }
             generationField();
    function generationField() {
        const div = D.querySelector('.field-content');
        for (let i = 8; i >= 1; i--)
            setBlockField(div, i)
    }

//********************************************** end .field ************************************************************
//********************************************** about *****************************************************************

         about();
function about(){
     let slides = D.querySelector('.slides'),arr = [1, 2, 3, 4, 5, 6],x,n,bool2,y,arrL = arr.length;
     arr.splice(arrL-1,1) //розставляємо елементи масиву на стартову позицію.
     arr.unshift(arrL)       //Довжина масиву може бути будь-яка, з додаванням нового відгуку просто треба запушити в кінець arrL+1
     //log(arr)       //[6, 1, 2, 3, 4, 5] замовчування масиву
   const setPic = (n, i) => {
           let div, pic = new Image();
               div = D.querySelector(`[data-id='${i}']`);
               div.appendChild(pic).classList.add('saddle-img');
                    pic.src = D.querySelector('#img' + n).getAttribute('src');
                    pic.alt = D.querySelector('#img' + n).getAttribute('alt');
                    pic.dataset.idPic = n;
         },
         getSaddle = (i) => {return D.querySelector(`[data-id='${i}']`)},  //отримуємо сідло
         getDataName = (i) => {return D.querySelector(`[data-name='${i}']`)},  //отримуємо блок з описом
         getChildDig = (i) => {return D.querySelector(`[data-id='${i}']`).children[0].getAttribute('data-id-pic')},//отримуємо цифру id картинки
         setSaddle = (i) => {let slides = D.querySelector('.slides');slides.insertAdjacentHTML('afterbegin', `<div data-id="${i}" class="saddle"></div>`)},//додаємо сідло
         anime = (arg,i) =>{
                      getSaddle(i).animate([
                          { transform: `translate(${arg}) rotateY(90deg)`},
                          { transform: 'rotateY(0)'},
                      ], {
                          duration: 520,
                          easing: 'ease-in-out'
                      });
        },
        boolFalse = () =>{           //частина логіки реверсу, що повторюється (основний крок вліво)
            x = arr[0];
            arr.splice(0,1);
            arr.push(x);
            n = arr[1];
        },
        boolArr = (bool,bool2) => {  //логіка реверсу
            if(bool){                //===============> праворуч
                if(bool2 === false){  //якщо щойно змінили напрямок
                    x = arr[0];
                    y = arr[1];
                    arr.splice(0,2);
                    arr.splice(arrL-1,0,x,y); //arr.splice(4,0,x,y)* //зірочкою позначено для візуалізації цифр методів з довжиною масиву 6
                    n = arr[1];
                }else{                                           //основний крок праворуч
                    x = arr[arrL - 1];   //x = arr[5]*
                    arr.splice(arrL - 1,1); //arr.splice(5,1)*
                    arr.unshift(x);
                    n = arr[1];
                }
            }
            if(!bool){              //<=============== ліворуч
                if(bool2 === undefined){                           //за замовчуванням початкова позиція задана для перегортання вправо,
                    arr.splice(0,1)                   //тому якщо спочатку починаємо вліво, змінюємо замовчування масиву
                    arr.push(arrL)
                    arr = arr.map(e => e + 2)
                    arr.splice(arrL - 2,2)
                    arr.splice(arrL - 2,0,1,2)  //[3, 4, 5, 6, 1, 2]*           //нове замовчування
                    boolFalse();                                //відразу робимо основний крок
                }else if(bool2 === true){                        //якщо щойно змінили напрямок
                    x = arr[arrL-2];                      //x = arr[4]*
                    y = arr[arrL-1];                      //x = arr[5]*
                    arr.splice(arrL-2,2);  //arr.splice(4,2)*
                    arr.splice(0,0,x,y);
                    n = arr[1];
                }
                if  (bool2 === false){    //основний крок вліво
                    boolFalse();
                }
            }
        },
        motion = (bool, position, offset) => {
             slides.removeEventListener('click', carousel);
               for (let i = 1; i <= 4 ;i++ ) {
                    let f = parseInt(getSaddle(i).style.left,10)           //**************************************
                        if(bool) {getSaddle(i).style.left = f + 100 + 'px'}
                            else {getSaddle(i).style.left = f - 100 + 'px'}                  //блок, який визначає позиції
                                 if (getSaddle(i).style.left === '200px') {
                                     getSaddle(i).style.top = '-15px';                        //*****************************
                                     remAct('.about-client');                           //видаляємо актив в описах
                                     getDataName(getChildDig(i)).classList.add('active'); //визначаємо та додаємо актив опису
                                 } else getSaddle(i).style.top = '0px'
                                 if (f === position) {
                                     getSaddle(i).animate([
                                         { transform: 'rotateY(90deg)' }
                                     ], {
                                         duration: 500,
                                         easing: 'ease-in-out'
                                     })
                                     setTimeout(()=>{getSaddle(i).remove()},480) //притримуємо видалення, щоб встигла відпрацювати анімація
                                         setTimeout(() => {
                                             boolArr(bool,bool2)    //реверс для n - id фотографії
                                           bool2 = bool === true;  //прапор, який змінює свій логічний стан при зміні напрямку зміщення слайдів
                                         setSaddle(i);            //сідло для фото
                                      setPic(n, i);              //вкидаємо в сідло фото
                                   getSaddle(i).style.left = offset + 'px'; //визначаємо куди кидати фотографію на початок чи кінець
                               if(bool){anime('-85px',i)}   //залежно з якого боку для анімації задаємо різні параметри
                            else{anime('85px',i)}
                        slides.addEventListener('click', carousel); //повертаємо обробник
                     }, 500, i);
                 }
             }
        },
        setStart = () =>{
            let j = 1, k = 1;
            for (let i = 0; i <= 300; i = i + 100) {
                getSaddle(j).style.left = `${i}` + 'px';
                setPic(k, j);
                j++;
                k++;
            }
        }

function carousel(e){
            if (e.target.closest('.behind')) {
                motion(true, 300, 0)     //якщо у право bool
            } else if (e.target.closest('.front')) {
                motion(false, 0, 300)     //якщо вліво
            }
        }

       setStart()             //задаємо початкове становище всіх картинок
       getSaddle(3).style.top = '-15px'; //задаємо зміщення вгору обраного за умовчанням користувача
       slides.addEventListener('click', carousel);
}

//********************************************** end .about ************************************************************
//********************************************** masonry ***************************************************************

   let double, ninefold, count = 7,
       msnry = new Masonry(".grid", {
       columnWidth: 372,
       itemSelector: ".grid-item",
       gutter: 19,
   });

const btnLoad2 = D.querySelector('.btn-load2'),
      div = D.querySelector('.grid'),
      setImgMas = (x, elem, section = '') => {
             let img = D.createElement('img');
                if (section === 'double') {
                       img.classList.add('grid-item--width2', 'grid-item--height2');
                       img.setAttribute('src', `images/masonry/masonry(${x}).jpg`);
                   } else if (section === 'ninefold') {
                       img.classList.add('grid-item--width3', 'grid-item--height3');
                       img.setAttribute('src', `images/masonry/block/masonry(${x}).jpg`);
                   } else {
                       img.classList.add('grid-item', 'item');
                       img.setAttribute('src', `images/masonry/masonry(${x}).jpg`);
                   }
                elem.appendChild(img);
             msnry.appended(img);
            msnry.layout();
      },
      msnryPreload = (i) => {
                      if (i <= 2) {setImgMas(i, div);}
                      if (i >= 3 && i <= 4) {
                          if (i === 3) {
                              double = D.createElement('div');
                              div.appendChild(double).classList.add('grid-item');
                              msnry.appended(double);
                              msnry.layout();
                          }
                          setImgMas(i, double, 'double');
                      }
                      if (i >= 5 && i <= 6) {setImgMas(i, div);}
                      if (i === 6) {
                          ninefold = D.createElement('div');
                          div.appendChild(ninefold).classList.add('grid-item');
                          msnry.appended(ninefold);
                          msnry.layout();
                      }
                      if (i > 6) {let a = i - 6;setImgMas(a, ninefold, 'ninefold');}
               let id = setTimeout(msnryPreload, 250, i = i + 1);if (i >= 16) {clearTimeout(id)}
      },
      refillImgMsnry = () => {
                let img = D.createElement('img');
                    img.setAttribute('src', `images/masonry/masonry(${count}).jpg`);
                    div.appendChild(img).classList.add('grid-item', 'item');
                    msnry.appended(img);
                    msnry.layout();
                    count++
                let id = setTimeout(refillImgMsnry, 250);
                if ((count - 13) % 6 === 0) {clearTimeout(id)}          //щоразу підвантажуємо по 6 картинок
                if (count === 43) {clearTimeout(id);btnLoad2.remove();}
      },
      setViewer = (e, a) => {
               let x = e.target.getAttribute('src').substring(e.target.getAttribute('src').length - 7).substring(2, 0).substring(0, 4).replace('(', '');
               div.insertAdjacentHTML('afterbegin', `<div class="test"><img class="img-test" src="./images/masonry${a}masonry(${x}).jpg" alt="test"></div>`);
      },
      focus = (bool, event) => {
             div.addEventListener(event, (e) => {
                 if (e.target.tagName === 'IMG' && (!e.target.closest('.grid-item--width3') || !e.target.closest('.grid-item--width2'))) {
                     (bool) ? e.target.classList.add('active') : e.target.classList.remove('active');
                 }
             })
         }

      focus(true, 'mouseover')
      focus(false, 'mouseout')

div.addEventListener('click', (e) => {
     if ((e.target.closest('.grid-item--width2') || e.target.closest('.grid-item')) && !(e.target.closest('.grid-item--width3'))) {setViewer(e, '/')}
     if (e.target.closest('.grid-item--width3')) {setViewer(e, '/block/')}
     if (e.target.closest('.img-test')) {D.querySelector('.test').remove()}
})

    msnryPreload(1);
    btnLoad2.addEventListener('click', refillImgMsnry);

    log('the end');

})(document, document.body);
